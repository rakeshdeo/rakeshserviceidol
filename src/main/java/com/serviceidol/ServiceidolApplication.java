package com.serviceidol;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = "com.serviceidol") 
public class ServiceidolApplication {

	public static void main(String[] args) {
		SpringApplication.run(ServiceidolApplication.class, args);
	}

}

