package com.serviceidol.controller;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.serviceidol.dto.CountryDto;
import com.serviceidol.service.CountryService;

//@CrossOrigin(origins = "http://localhost:4200")
@RestController
//@RequestMapping("/api")
public class CountryController {

	@Autowired
	CountryService countryService;

	@RequestMapping(value = "/country", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<Long> saveCountry(@RequestBody CountryDto countryDto) {
		Long id = 0L;
		if (countryDto != null) {
			id = countryService.saveCountry(countryDto);
			System.out.println("id...." + id);
		}
		return new ResponseEntity<>(id, HttpStatus.CREATED);
	}

	@GetMapping("/countries/{id}")
	public ResponseEntity<CountryDto> getCountryById(@PathVariable("id") Long id) {
		CountryDto dto = null;
		try {
			dto = countryService.getCountryById(id);
		} catch (EntityNotFoundException e) {
			 e.getMessage();
		}
		return new ResponseEntity<>(dto, HttpStatus.OK);
	}

	@GetMapping("/allcountries")
	public ResponseEntity<List<CountryDto>> getAllCountries() {
		List<CountryDto> response = new ArrayList<>();
		response = countryService.getAllCountries();
		return new ResponseEntity<>(response, HttpStatus.OK);

	}

	@DeleteMapping("/country/{id}")
	public ResponseEntity<Void> deleteCountry(@PathVariable("id") Long id) {
		countryService.deleteCountry(id);
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@PutMapping("/updatecountry")
	public ResponseEntity<CountryDto> updateCountry(@RequestBody CountryDto countryDto){
		CountryDto dto = null;
		if(countryDto != null) {
			try {
				dto = countryService.updateCountry(countryDto);
			}catch(EntityNotFoundException e) {
				e.getMessage();
			}			
		}
		return new ResponseEntity<>(dto, HttpStatus.OK);		
	}

}
