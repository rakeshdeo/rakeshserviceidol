package com.serviceidol.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.serviceidol.dto.StateDto;
import com.serviceidol.response.CountryStateResponse;
import com.serviceidol.response.StateResponse;
import com.serviceidol.service.StateService;

//@CrossOrigin(origins = "http://localhost:4200")
@RestController
//@RequestMapping("/api")
public class StateController {

	@Autowired
	private StateService stateService;

	@PostMapping("/states")
	public ResponseEntity<Long> saveState(@RequestBody StateDto stateDto) {
		Long stateId = 0L;
		try {
			if (stateDto != null)
				stateId = stateService.saveState(stateDto);
		} catch (Exception e) {
			e.getMessage();
		}
		return new ResponseEntity<>(stateId, HttpStatus.CREATED);
	}

	@GetMapping("allstates")
	public ResponseEntity<List<StateResponse>> getAllStates() {
		List<StateResponse> response = null; //new CountryStateResponse();
		try {
			response = stateService.getAllStates();
			System.out.println("StateController..." + response);
		} catch (Exception e) {
			e.getMessage();
		}
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

}
