package com.serviceidol.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.serviceidol.model.Country;

@Repository
public interface CountryRepository extends JpaRepository<Country, Long> {
	
	Country findCountryById(Long id);

}
