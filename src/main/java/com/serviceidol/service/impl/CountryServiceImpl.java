package com.serviceidol.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.serviceidol.dto.CountryDto;
import com.serviceidol.model.Country;
import com.serviceidol.repositories.CountryRepository;
import com.serviceidol.service.CountryService;
import com.serviceidol.util.DateUtility;

@Service
public class CountryServiceImpl implements CountryService {

	@Autowired
	private CountryRepository countryRepository;

	@Override
	public Long saveCountry(CountryDto countryDto) {
		// TODO Auto-generated method stub
		Country country = new Country();
		if (countryDto != null && countryDto.getName() != null) {
			country.setName(countryDto.getName());
			country.setCreatedDate(new Date());
			countryRepository.save(country);
		}
		return country.getId();
	}

	@Override
	public List<CountryDto> getAllCountries() {
		// TODO Auto-generated method stub
		List<Country> country = countryRepository.findAll();

		List<CountryDto> list = new ArrayList<>();
		if (country != null) {
			for (Country c : country) {
				CountryDto dto = new CountryDto();
				dto.setId(c.getId());
				dto.setName(c.getName());
				dto.setCreatedDate(DateUtility.convertDate(c.getCreatedDate()));
				if (c.getUpdatedDate() != null) {
					dto.setUpdatedDate(DateUtility.convertDate(c.getUpdatedDate()));
				}

				list.add(dto);
			}
		}
		return list;
	}

	@Override
	public CountryDto getCountryById(Long id) {
		CountryDto dto = new CountryDto();
		if (id > 0) {
			Country country = countryRepository.getOne(id);
			if (country != null) {
				dto.setId(country.getId());
				dto.setName(country.getName());
				dto.setCreatedDate(DateUtility.convertDate(country.getCreatedDate()));
				if (country.updatedDate != null)
					dto.setUpdatedDate(DateUtility.convertDate(country.getUpdatedDate()));
			}
		}
		return dto;
	}

	@Override
	public CountryDto updateCountry(CountryDto countryDto) {
		CountryDto dto = new CountryDto();
		if (countryDto != null && countryDto.getId() > 0) {
			Country country = countryRepository.getOne(countryDto.getId());
			if (country != null) {
				country.setName(countryDto.getName());
				country.setUpdatedDate(new Date());
				countryRepository.saveAndFlush(country);
				dto = getCountryEditedValue(country);
			}
		}
		return dto;
	}

	public CountryDto getCountryEditedValue(Country country) {
		CountryDto dto = new CountryDto();
		if(country != null) {
			dto.setId(country.getId());
			dto.setName(country.getName());
			dto.setCreatedDate(DateUtility.convertDate(country.getCreatedDate()));
			dto.setUpdatedDate(DateUtility.convertDate(country.getUpdatedDate()));
		}
		return dto;
	}
	
	@Override
	public void deleteCountry(Long id) {
		if (id > 0) {
			Country country = countryRepository.getOne(id);
			if (country != null)
				countryRepository.delete(country);
		}

	}

}
