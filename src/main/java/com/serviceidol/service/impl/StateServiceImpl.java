package com.serviceidol.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.serviceidol.dto.CountryDto;
import com.serviceidol.dto.StateDto;
import com.serviceidol.model.Country;
import com.serviceidol.model.State;
import com.serviceidol.repositories.CountryRepository;
import com.serviceidol.repositories.StateRepository;
import com.serviceidol.response.CountryStateResponse;
import com.serviceidol.response.StateResponse;
import com.serviceidol.service.StateService;

@Service
public class StateServiceImpl implements StateService {

	@Autowired
	private CountryRepository countryRepository;

	@Autowired
	private StateRepository stateRepository;

	@Override
	public Long saveState(StateDto stateDto) {
		StateDto dto = new StateDto();
		Long stateId = 0L;
		if (stateDto != null && stateDto.getName() != null) {
			Country country = countryRepository.findCountryById(stateDto.getCountryId());
			if (country != null) {
				State state = new State();
				state.setCountry(country);
				state.setName(stateDto.getName());
				state.setCreatedDate(new Date());
				stateRepository.save(state);
				stateId = state.getId();
			}
		}
		return stateId;
	}

	@Override
	public List<StateResponse> getAllStates() {
		List<CountryDto> countryList = new ArrayList<>();		
		List<StateResponse> stateResponses = new ArrayList<>();		
		CountryStateResponse countryStateResponse = new CountryStateResponse();		
		List<State> states = stateRepository.findAll();
		
		if (states != null) {
			Map<Country, List<State>> stateMap = states.stream().collect(Collectors.groupingBy(State::getCountry));
			
			List<CountryDto> listCountryDto = new ArrayList<>();
			
			String stateName="";
			//StateResponse stateResponse = null;
			for (Map.Entry<Country, List<State>> entry : stateMap.entrySet()) {
				StateResponse stateResponse = new StateResponse();
				CountryDto cDto = new CountryDto();
				
				Long cId = entry.getKey().getId();
				String cName = entry.getKey().getName();
				cDto.setId(cId);
				cDto.setName(cName);
				listCountryDto.add(cDto);
				
				List<State> myStates = entry.getValue();
				List<StateDto> listStates = new ArrayList<>();
				for (State s : myStates) {
					StateDto stateDto = new StateDto();
					stateDto.setName(s.getName());
					stateName = s.getName();
					listStates.add(stateDto);	
				}		
				System.out.println("CountryName..." + cName); 
				System.out.println("stateSize..." + listStates.size() + "..stateName.."+stateName); 
				System.out.println("listCountryDto..." + listCountryDto.size());
				
				stateResponse.setCountry(cDto);
				stateResponse.setStates(listStates);
				
				stateResponses.add(stateResponse);
				
			}
			//System.out.println("stateSizeOuuter..." + listStates.size());

		} 
		return stateResponses;
	}

	@Override
	public StateDto getStateById(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public StateDto updateState(State state) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void deleteState(Long id) {
		// TODO Auto-generated method stub

	}

}
