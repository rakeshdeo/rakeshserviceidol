package com.serviceidol.service;

import java.util.List;

import com.serviceidol.dto.CountryDto;
import com.serviceidol.model.Country;

public interface CountryService {
	
	public Long saveCountry(CountryDto countryDto);
	
	public List<CountryDto> getAllCountries();
	
	public CountryDto getCountryById(Long id);
	
	public CountryDto updateCountry(CountryDto countryDto);
	
	public void deleteCountry(Long id);

}
