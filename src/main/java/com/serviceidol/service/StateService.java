package com.serviceidol.service;

import java.util.List;

import com.serviceidol.dto.StateDto;
import com.serviceidol.model.State;
import com.serviceidol.response.CountryStateResponse;
import com.serviceidol.response.StateResponse;

public interface StateService {
	
	public Long saveState(StateDto stateDto);
	
	public List<StateResponse> getAllStates();

	public StateDto getStateById(Long id);
	
	public StateDto updateState(State state);
	
	public void deleteState(Long id);

}
