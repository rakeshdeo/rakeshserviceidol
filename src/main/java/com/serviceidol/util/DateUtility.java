package com.serviceidol.util;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtility {

	/**
	 * convert Date date type to dd/MM/YYYY(e.g.25/12/2019)
	 */
	public static String convertDate(Date date) {

		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/YYYY");
		String dstrDate = dateFormat.format(date);
		return dstrDate;
	}

}
