package com.serviceidol.response;

import java.util.List;

import com.serviceidol.dto.CountryDto;
import com.serviceidol.dto.StateDto;

public class CountryStateResponse {
	
	private List<CountryDto> countryList;
	private List<StateResponse> stateResponseList;
	
	public List<CountryDto> getCountryList() {
		return countryList;
	}
	public void setCountryList(List<CountryDto> countryList) {
		this.countryList = countryList;
	}
	public List<StateResponse> getStateResponseList() {
		return stateResponseList;
	}
	public void setStateResponseList(List<StateResponse> stateResponseList) {
		this.stateResponseList = stateResponseList;
	}
	
	
}
