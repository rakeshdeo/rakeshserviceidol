package com.serviceidol.response;

import java.util.List;

import com.serviceidol.dto.CountryDto;
import com.serviceidol.dto.StateDto;

public class StateResponse {
	
	private CountryDto country;
	private List<StateDto> states;
	
	public CountryDto getCountry() {
		return country;
	}
	public void setCountry(CountryDto country) {
		this.country = country;
	}
	public List<StateDto> getStates() {
		return states;
	}
	public void setStates(List<StateDto> states) {
		this.states = states;
	}
	
	


}
